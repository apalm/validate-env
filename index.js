#!/usr/bin/env node

const arg = require("arg");
const validate = require("./src/validate");

if (require.main === module) {
  const cliOpts = arg({
    "--env-file-path": String,
    "-e": "--env-file-path",
    "--json-schema-path": String,
    "-s": "--json-schema-path"
  });

  validate({
    envFilePath: cliOpts["--env-file-path"],
    jsonSchemaPath: cliOpts["--json-schema-path"]
  })
    .then(result => {
      if (!result.valid) {
        // eslint-disable-next-line no-console
        console.error(JSON.stringify(result.errors, null, 2));
        process.exit(1);
      }
      process.exit(0);
    })
    .catch(reason => {
      // eslint-disable-next-line no-console
      console.error(reason);
      process.exit(1);
    });
}

module.exports = validate;
