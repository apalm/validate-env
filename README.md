# validate-env

Validate an .env file against a JSON schema.

## CLI

If you don't want to install globally, and you have `npx` on your machine:

```sh
npx git+ssh://git@gitlab.technicity.io:general/validate-env.git OPTIONS
```

Otherwise:

```sh
npm install -g git+ssh://git@gitlab.technicity.io:general/validate-env.git
# Or: yarn global add git+ssh://git@gitlab.technicity.io:general/validate-env.git
validate-env OPTIONS
```

### Options

| Option             | Alias | Required? | Default | Description                                     |
| ------------------ | ----- | --------- | ------- | ----------------------------------------------- |
| --env-file-path    | -e    | Yes       |         | Path to .env file                               |
| --json-schema-path | -s    | Yes       |         | Path to JSON schema file. Must be .json or .js. |

## Node.js API

```js
const path = require("path");
const validateEnv = require("validate-env");

// Options are the same as the (non-aliased) options above, but camel-cased.
validateEnv({
  envFilePath: path.join(__dirname, ".env"),
  jsonSchemaPath: path.join(__dirname, "foo.json")
}).then(({ valid, errors }) => {
  // Do whatever.
});
```
