const fs = require("fs");
const util = require("util");
const path = require("path");
const dotenv = require("dotenv");
const Ajv = require("ajv");

const readFile = util.promisify(fs.readFile);
const ajv = new Ajv({ allErrors: true, useDefaults: true });

const optsSchema = {
  type: "object",
  properties: {
    envFilePath: { type: "string" },
    jsonSchemaPath: { type: "string", pattern: "\\.(js|json)$" }
  },
  required: ["envFilePath", "jsonSchemaPath"]
};

module.exports = async function validate(opts) {
  const areOptsValid = ajv.validate(optsSchema, opts);
  if (!areOptsValid) {
    throw new Error(JSON.stringify(ajv.errors, null, 2));
  }

  const env = dotenv.parse(
    await readFile(path.resolve(opts.envFilePath), "utf8")
  );
  const valid = ajv.validate(require(path.resolve(opts.jsonSchemaPath)), env);
  if (!valid) {
    return { valid: false, errors: ajv.errors };
  }
  return { valid: true, errors: [] };
};
