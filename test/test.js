/* eslint-env jest */

const path = require("path");
const util = require("util");
const cp = require("child_process");
const validate = require("../index");

const exec = util.promisify(cp.exec);

describe("validate-env", () => {
  test("Node.js API returns correct validation status and no errors for valid .env", async () => {
    const result = await validate({
      envFilePath: path.join(__dirname, "fixtures", "good.env"),
      jsonSchemaPath: path.join(__dirname, "fixtures", "schema.json")
    });
    expect(result).toMatchSnapshot();
  });

  test("Node.js API returns correct validation status and errors for invalid .env", async () => {
    const result = await validate({
      envFilePath: path.join(__dirname, "fixtures", "bad.env"),
      jsonSchemaPath: path.join(__dirname, "fixtures", "schema.json")
    });
    expect(result).toMatchSnapshot();
  });

  test("Node.js API throws on invalid options", async () => {
    expect(validate({ jsonSchemaPath: "bar" })).rejects.toMatchSnapshot();
  });

  test("CLI works for valid .env", async () => {
    const { stderr } = await exec(
      [
        "node",
        "index.js",
        "-e",
        path.join(__dirname, "fixtures", "good.env"),
        "-s",
        path.join(__dirname, "fixtures", "schema.json")
      ].join(" ")
    );
    expect(stderr).toBe("");
  });

  test("CLI exits for invalid .env", async () => {
    expect(() =>
      cp.execSync(
        [
          "node",
          "index.js",
          "-e",
          path.join(__dirname, "fixtures", "bad.env"),
          "-s",
          path.join(__dirname, "fixtures", "schema.json")
        ].join(" ")
      )
    ).toThrow();
  });
});
